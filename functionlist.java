import java.util.ArrayList;

class functionlist {
  private String function;
  int function_start;
  private ArrayList<functionlist> children = new ArrayList<functionlist>();
  private int max_recursion;
  private int current_recursion;
  private table t;
  private latencyinfo li;
  
  functionlist(String function, int function_start, int max_recursion, int current_recursion, table t, latencyinfo li){
    this.function = function;
    this.function_start = function_start;
    this.max_recursion = max_recursion;
    this.current_recursion = current_recursion + 1;
    this.t = t;
    this.li = li;
  }
  
  void add_child_functions() {
    functionlist fl;
    int current_index = function_start;
    line l = t.get_line(current_index);
    if(!l.has_children()){
      return;
    }
    String cpu = l.get_cpu();
    String pid = l.get_pid();
    int function_end = t.find_function_end(current_index); 
    int current_function_end;
    latencyinfo li;
    current_index = t.next_pid_function(current_index, cpu, pid);
    while(current_index < function_end) {
      current_function_end = t.find_function_end(current_index);
      l = t.get_line(current_index);
      li = new latencyinfo(current_index, current_function_end, t.get_latency(current_index), l.get_pid()); 
      fl = new functionlist(l.get_function(), current_index, max_recursion, current_recursion, t, li);
      children.add(fl);
      if(l.has_children()){
        current_index = current_function_end;
      }
      current_index = t.next_pid_function(current_index, cpu, pid);      
    }
  } 

  void populate_children() {
    if(max_recursion == current_recursion){
      return;   
    }
    add_child_functions();
    for(functionlist fl: children){
      fl.populate_children();
    }
  }

  void print_functionlist(){
    String s = new String(new char[current_recursion -1]).replace("\0", "\t");
    System.out.print(s+function+" ");
    li.print_li();
    System.out.println();
    for(functionlist fl: children){
      fl.print_functionlist();
    }
  }
}  
  
  