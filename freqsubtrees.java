import java.util.ArrayList;
import java.lang.Integer;
import java.util.List;

class freqsubtrees {
  
  private functree_to_tree ftt;
  table t;
  ArrayList<String> functions;
  
  freqsubtrees(table t, ArrayList<String> functions){
    this.t = t;
    this.functions = functions;
  }
  
  freqsubtrees(){
  }
  
  public static void main(String[] args) {
    freqsubtrees test = new freqsubtrees();
    if(test.parse_args(args)){
      return;
    }
    files f = new files();
    f.read_file(args[0]);
    table t = f.get_table();
    freqsubtrees ftt = new freqsubtrees(t, t.matching_functions(args[1]));
    ftt.populate_ftt();
    for(String s: ftt.common_subtrees(Integer.parseInt(args[2]))){
      ftt.print_tree(s);
      System.out.println();
    }
  }
  
  boolean parse_args(String[] args){
    if(args.length == 0){
      print_help();
      return true;
    }
    if(args.length != 3) {
      System.out.println("Invalid option");
      System.out.println();
      System.out.println("freqsubtrees:");
      print_help();
      return true;
    }
    return false;    
  }
  
  void print_help(){
    System.out.println("This shows the sequence of function calls that occur most frequently within a specified function.");
    System.out.println();
    System.out.println("usage: ");
    System.out.println("java -cp .;lang3.jar freqsubtrees <tracing_data> <function> <number>");
    System.out.println("tracing_data: A text file which contains the output of trace-cmd's function graph tracer.");
    System.out.println("function: The function about which information will be presented (must include the end brackets).");
    System.out.println("number: The number of subtrees that will be shown (so given a number n, the top n most common subtrees will be outputted).");
  }
    
  
  void populate_ftt() {
    int current_index;
    int next_func;
    ArrayList<functree> treelist = new ArrayList<functree>();
    functree ft;
    for(String func: functions){
      current_index = 0;
      next_func = t.next_occurence(current_index, func);
      
      while(next_func != -1){
        current_index = next_func;
        ft = new functree(func, current_index, t);
        ft.populate_children();
        treelist.add(ft);
        current_index++;
        next_func = t.next_occurence(current_index, func);
      }
    }
    ftt = new functree_to_tree(treelist);
  }  
  
  void print_tree(String tree){
    String tabs;
    int count = 0;
    boolean previous_dash = false;
    String[] leaves = tree.split(" ");
    for(String s: leaves) {
      if(!s.equals("-")){
        if(previous_dash == false){
          count++;
        }
        previous_dash = false;
        tabs = new String(new char[count-1]).replace("\0", "\t");
        System.out.print(tabs);
        System.out.println(s);
      }
      else{
        if(previous_dash == true){
          count--;
        }
        previous_dash = true;
      }
    }
  }
  
  int count_start(){
    int max = 0;
    int count = 0;
    for(String function: functions){
      count = t.count_function_calls(function);
      if(count > max){      
        max = count;
      }
    }
    return (max/2);
  }
  
  ArrayList<String> common_subtrees(int limit){
    int count = count_start();
    ArrayList<String> subtrees = ftt.get_trees(count);
    ArrayList<String> previous_subtrees = subtrees;
    while(subtrees.size() >= limit){
      previous_subtrees = subtrees;
      count++;
      subtrees = ftt.get_trees(count);
    }
    return previous_subtrees;
  }
}
    