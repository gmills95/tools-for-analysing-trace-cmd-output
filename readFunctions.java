import java.io.PrintWriter;
import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

class readFunctions {
  private ArrayList<String> functions = new ArrayList<String>();
  private String filename;
  
  readFunctions(String filename) {
    this.filename = filename;
  }
  
  public static void main(String[] args) {
    readFunctions rf = new readFunctions("camflowunique2.txt");
    rf.read_file();
    rf.print_functions();
  }

  void read_file() {
    File f = new File(filename);
    Scanner s;
    try{
      s = new Scanner(f);
      read_lines(s);
    }
    catch(Exception e){
      throw new Error("File does not exist");
    }
  }    
  
  
  void read_lines(Scanner s){
    while(true){
      try{
        String data = s.nextLine();
          functions.add(data);
      }
      catch(Exception e){
        return;
      }
    }
  }  
  
  void print_functions() {
    for(String s:functions) {
      System.out.println(s);
    }
  }
  
  ArrayList<String> get_functions() {
    return functions;
  }
}