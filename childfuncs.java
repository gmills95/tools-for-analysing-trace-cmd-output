import java.util.ArrayList;
import java.lang.Integer;

class childfuncs {
  private String function;
  private ArrayList<functionlist> children = new ArrayList<functionlist>();
  private table t;
  private int max_recursion;
  private ArrayList<latencyinfo> latencies = new ArrayList<latencyinfo>();
  
  childfuncs(String function, table t, int max_recursion) {
    this.function = function;
    this.t = t;
    this.max_recursion = max_recursion;
  }
  
  childfuncs() {
  }
  
  public static void main(String[] args) {
    childfuncs test = new childfuncs();
    //test.run();
    if(test.parse_args(args)){
      return;
    }
    files f = new files();
    f.read_file(args[0]);
    childfuncs cf = new childfuncs(args[1], f.get_table(), Integer.parseInt(args[2]));
    cf.populate_latencies();
    cf.populate_children();
    cf.print_childfuncs();
  }
  
  ArrayList<functionlist> get_children() {
    return children;
  }
  
  boolean parse_args(String[] args){
    if(args.length == 0){
      print_help();
      return true;
    }
    if(args.length != 3) {
      System.out.println("Invalid option");
      System.out.println();
      System.out.println("childfuncs:");
      print_help();
      return true;
    }
    if(!args[1].endsWith("()")){
      System.out.println("Function name must include brackets");
      return true;
    }
    return false;    
  }
  
  void print_help() {
    System.out.println("This presents information about every instance of the specified function, as well as showing all the functions that it calls during its execution.");
    System.out.println();
    System.out.println("usage:"); 
    System.out.println("java childfuncs <tracing_data> <function> <depth>");
    System.out.println("tracing_data: A text file which contains the output of trace-cmd's function graph tracer.");
    System.out.println("function: The function about which information will be presented (must include the end brackets).");
    System.out.println("depth: The number of levels of output that will be shown. (A depth of 1 would show output about the specified function only, a depth of 2 would show the child functions that it calls)");
  }
    
  
  /*void run() {
    files f = new files();
    f.read_file("test.txt");
    t = f.get_table();
    function = "__fdget_pos()";
    functionlist fl = get_childfuncs(1);
    assert(fl.size() == 2);
    assert(fl.get(1).equals("mutex_lock()"));
    populate_latencies();
    populate_children();
    assert(children.size() == 2 && latencies.size() == 2);
  }*/
    
  
  void populate_children() {
    functionlist fl;
    for(latencyinfo li:latencies){
      fl = new functionlist(function, li.get_start(), max_recursion, 0, t, li);
      children.add(fl);
    }
    for(functionlist f: children){
      f.populate_children();
    }
  }
  
  void populate_latencies() {
    latency l = new latency(t, function);
    l.populate_latencies();
    latencies = l.get_latencies();
  }
    
  void print_childfuncs() {
    for(functionlist fl: children){
      fl.print_functionlist();
    }
  }
  
}