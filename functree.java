import java.util.ArrayList;

class functree {
  private String function;
  int function_start;
  private ArrayList<functree> children = new ArrayList<functree>();
  private boolean has_children;
  private table t;
  private latencyinfo li;  
  
  functree(String function, int function_start, table t) {
    this.function = function;
    this.function_start = function_start;
    this.t = t;
    has_children = t.get_line(function_start).has_children();
  }
  
  public static void main(String[] args) {
    files f = new files();
    f.read_file("test.txt");
    functree ft = new functree("__fdget_pos()", 1, f.get_table());
    ft.populate_children();
    ft.print_functree();
  }    
  
  void add_child_functions() {
    if(!has_children){
      return;
    }
    functree ft;
    int current_index = function_start;
    line l = t.get_line(current_index);
    String cpu = l.get_cpu();
    String pid = l.get_pid();
    int function_end = t.find_function_end(current_index); 
    int current_function_end;
    current_index = t.next_pid_function(current_index, cpu, pid);
    while(current_index < function_end) {
      current_function_end = t.find_function_end(current_index);
      l = t.get_line(current_index); 
      ft = new functree(l.get_function(), current_index, t);
      children.add(ft);
      if(l.has_children()){
        current_index = current_function_end;
      }
      current_index = t.next_pid_function(current_index, cpu, pid);      
    }
  }

  void populate_children() {
    add_child_functions();
    for(functree ft: children){
      ft.populate_children();
    }
  }

  void print_functree() {
    System.out.println(function);
    if(has_children){
      System.out.println("-");
    }
    for(functree ft: children){
      ft.print_functree();
    }
  } 
  
  String get_function(){
    return function;
  }
  
  ArrayList<functree> get_children(){
    return children;
  }
  
  boolean has_children(){
    return has_children;
  }
}  