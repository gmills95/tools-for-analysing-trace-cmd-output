import java.util.ArrayList;

class latcomp {
  private ArrayList<latencyinfo> parent_lats;
  private ArrayList<latencyinfo> child_lats;
  private ArrayList<overheadinfo> overheads = new ArrayList<overheadinfo>();
  
  latcomp(ArrayList<latencyinfo> parent_lats, ArrayList<latencyinfo> child_lats) {
    this.parent_lats = parent_lats;
    this.child_lats = child_lats;
  }
  
  latcomp() {
  }
  
  public static void main(String[] args) {
    latcomp test = new latcomp();
    test.run();
    if(test.parse_args(args)){
      return;
    }

    files f = new files();
    f.read_file(args[0]);
    latency l1 = new latency(f.get_table(), args[1]);
    latency l2 = new latency(f.get_table(), args[2]);
    l1.populate_latencies();
    l2.populate_latencies();
    latcomp lc = new latcomp(l1.get_latencies(), l2.get_latencies());
    lc.populate_overheads();
    lc.print_overheads();
  }
  
  boolean parse_args(String[] args){
    if(args.length == 0){
      print_help();
      return true;
    }
    if(args.length != 3) {
      System.out.println("Invalid option");
      System.out.println();
      System.out.println("latcomp:");
      print_help();
      return true;
    }
    if(!args[1].endsWith("()") || !args[2].endsWith("()")){
      System.out.println("Function name must include brackets");
      return true;
    }
    return false;
  } 

  void print_help() {
    System.out.println("This shows every instance of the specified parent function in which the specified child function is called. On top of that, the percentage of the instance of the parent function the is taken up by the child function is shown.");
    System.out.println();
    System.out.println("usage:");
    System.out.println("java latcomp <tracing_data> <parent_function> <child_function>");
    System.out.println("tracing_data: A text file which contains the output of trace-cmd's function graph tracer.");
    System.out.println("parent_function: The name of the parent function within the tracing data (must include the end brackets).");
    System.out.println("child_function: The name of the child function within the tracing data (must include brackets).");
  }
        
  
  void run() {
    files f = new files();
    f.read_file("test.txt");
    latency l1 = new latency(f.get_table(), "__fdget_pos()");
    latency l2 = new latency(f.get_table(), "__fget()");
    l1.populate_latencies();
    l2.populate_latencies();
    parent_lats = l1.get_latencies();
    child_lats = l2.get_latencies();
    populate_overheads();
    assert(overheads.size() == 3);
    double d = (0.912/8.371)*100;
    Double d1 = new Double(overheads.get(1).get_overhead());
    Double d2 = new Double(d);
    assert(d1.equals(d2));
  }
  
  int find_index(latencyinfo li) {
    latencyinfo parent_lat;
    int start = li.get_start();
    int end = li.get_end();
    String pid = li.get_pid();
    for(int i = 0; i < parent_lats.size(); i++){
      parent_lat = parent_lats.get(i);
      if(parent_lat.get_start() < start && parent_lat.get_end() > end && pid.equals(parent_lat.get_pid())) {
        return i;
      }
    }
    return -1;
  }
  
  double calculate_overhead(latencyinfo parent_lat, latencyinfo child_lat) {
    return (child_lat.get_lat() / parent_lat.get_lat()) * 100;
  }
  
  int find_overheads_index(int i) {
    for(int j = 0; j < overheads.size(); j++){
      if(overheads.get(j).get_index() == i){
        return j;
      }
    }
    return -1;
  }
  
  void populate_overheads() {
    int parent_index;
    int overheads_index;
    double overhead;
    double child_lat;
    for(latencyinfo li: child_lats){
      parent_index = find_index(li);
      if(parent_index != -1){
        child_lat = li.get_lat();
        overhead = calculate_overhead(parent_lats.get(parent_index), li);
        overheads_index = find_overheads_index(parent_index);
        if(overheads_index == -1){
          overheads.add(new overheadinfo(parent_index, overhead, child_lat));
        }
        else{
          overheads.get(overheads_index).add_overhead(overhead, child_lat);
        }
      }
    }
  }
  
  void print_overheads() {
    for(int i = 0; i < overheads.size(); i++){
      parent_lats.get(i).print_li();
      overheads.get(i).print_overhead();
    }
  }
  
  double average_overhead() {
    if(overheads.size() == 0){
      return -1;
    }
    double sum = 0;
    for(overheadinfo o: overheads){
      sum += o.get_overhead();
    }
    return sum/overheads.size();
  }    
}
    
      
  
  
