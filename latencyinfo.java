class latencyinfo {
  
  private int start;
  private int end;
  private double lat;
  private String pid;
  
  latencyinfo(int start, int end, double lat, String pid){
    this.start = start;
    this.end = end;
    this.lat = lat;
    this.pid = pid;
  }
  
  public static void main(String[] args) {
  }
  
  void print_li() {
    System.out.print(pid+" " +start+" "+end+" "+lat+" ");
  }
  
  int get_end() {
    return end;
  }
  
  int get_start() {
    return start;
  }
  
  double get_lat() {
    return lat;
  }
  
  String get_pid() {
    return pid;
  }
}  