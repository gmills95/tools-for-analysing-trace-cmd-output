import java.util.ArrayList;
import java.util.regex.Pattern;

class average_overheads {
  
  table t;
  private ArrayList<String> functions;
  private String parent_func;
  
  average_overheads(table t, ArrayList<String> functions, String parent_func) {
    this.t = t;
    this.functions = functions;
    this.parent_func = parent_func;
  }
  
  average_overheads(){
  }
  
  public static void main(String[] args) {
    average_overheads test = new average_overheads();
    if(test.parse_args(args)){
      return;
    }      
    files f = new files();
    f.read_file(args[0]);
    readFunctions rf = new readFunctions(args[1]);
    rf.read_file();
    average_overheads ao = new average_overheads(f.get_table(), rf.get_functions(), args[2]);
    if(args.length == 3){
      ao.print_overheads();
      return;
    }
    ao.print_overheads(args[3]);
  }
  
  boolean parse_args(String[] args){
    if(args.length == 0){
      System.out.println("This finds the average overhead imposed by each function from a supplied list of functions on a specified parent function within a given file of tracing data. Optionally, the user can specify a regular expression. Then, only information about functions with matching names will be presented.");
      System.out.println();      
      System.out.println("usage:");
      System.out.println("java average_overheads <tracing_data> <function_list> <parent_function> [<regex>]");
      System.out.println("tracing_data: A text file which contains the output of trace-cmd's function graph tracer.");
      System.out.println("function_list: A text file which contains a list of function names, one per line.");
      System.out.println("parent_function: The name of the parent function within the tracing data (must include the end brackets).");
      System.out.println("regex: A regular expression, information will only be given about functions with matching names.");
      return true;
    }
    if(args.length != 3 && args.length != 4) {
      System.out.println("Invalid option, run with no arguments to see help");
      return true;
    }
    if(!args[2].endsWith("()")){
      System.out.println("Function name must include brackets");
      return true;
    }
    return false;
  }
  
  void print_overheads() {
    latency parent_latency = new latency(t, parent_func);
    parent_latency.populate_latencies();
    ArrayList<latencyinfo> parent_lats = parent_latency.get_latencies();
    latency child_latency;
    latcomp lc;
    for(String s:functions){
      child_latency = new latency(t, s);
      child_latency.populate_latencies();
      lc = new latcomp(parent_lats, child_latency.get_latencies());
      lc.populate_overheads();
      double overhead = lc.average_overhead();
      if(overhead > 0) {
        System.out.printf("%s : %.2f \n", s, lc.average_overhead());
      }
    }
  }
  
  void print_overheads(String pattern) {
    latency parent_latency = new latency(t, parent_func);
    parent_latency.populate_latencies();
    ArrayList<latencyinfo> parent_lats = parent_latency.get_latencies();
    latency child_latency;
    latcomp lc;
    for(String s:functions){
      if(Pattern.matches(pattern, s)){
        child_latency = new latency(t, s);
        child_latency.populate_latencies();
        lc = new latcomp(parent_lats, child_latency.get_latencies());
        lc.populate_overheads();
        double overhead = lc.average_overhead();
        if(overhead > 0) {
          System.out.printf("%s : %.2f \n", s, lc.average_overhead());
        }
      }
    }
  }
}
      

    