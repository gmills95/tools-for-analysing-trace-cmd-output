import java.text.DecimalFormat;

class overheadinfo {
  private int index;
  private double overhead;
  private double lat;
  
  overheadinfo(int index, double overhead, double lat) {
    this.index = index;
    this.overhead = overhead;
    this.lat = lat;
  }
  
  public static void main(String[] args) {
  }

  void add_overhead(double new_overhead, double new_lat) {
    overhead += new_overhead;
    lat += new_lat;
  }
  
  int get_index() {
    return index;
  }
  
  double get_overhead() {
    return overhead;
  }
  
  void print_overhead() {
    DecimalFormat df = new DecimalFormat("#.##");
    System.out.println(df.format(lat) + " " + df.format(overhead));
  }
}
    