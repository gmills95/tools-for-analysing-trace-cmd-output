import java.util.ArrayList; 

class table {
  ArrayList<line> lines= new ArrayList<line>();
  
  public static void main(String[] args) {
    
  }
  
  void add_line(line l) {
    lines.add(l);
  }
  
  line get_line(int i) {
    return lines.get(i);
  }
  
  ArrayList<String> matching_functions(String pattern){
    ArrayList<String> func_list = new ArrayList<String>();
    String function;
    for(line l: lines){
      function = l.get_function();
      if(function.matches(pattern) && !string_in_list(func_list, function)){
        func_list.add(function);
      }
    }
    return func_list;
  }
  
  boolean string_in_list(ArrayList<String> list, String s){
    for(String word: list){
      if(word.equals(s)){
        return true;
      }
    }
    return false;
  }
  
  int count_function_calls(String func){
    int current_index = 0;
    int next_func = next_occurence(current_index, func);
    int count = 0;
    while(next_func != -1){
      current_index = next_func;
      count++;
      current_index++;
      next_func = next_occurence(current_index, func);
    }
    return count;
  }
  
  int next_occurence(int i, String func){
    int index = i;
    int len = lines.size();
    while(i < len){
      line l = get_line(i);
      if(l.get_function().startsWith(func)){
          return i;
        }
      i++;
    }
    return -1;
  }
  
  double get_latency(int i) {
    String cpu = get_line(i).get_cpu();
    String pid = get_line(i).get_pid();
    int index = find_function_end(i);
    return get_line(index).get_latency();
  }
  
  int find_function_end(int i) {
    int brackets = 1;
    int current_index = i;
    line l = get_line(current_index);
    String cpu = l.get_cpu();
    String pid = l.get_pid();
    if(!l.has_children()){
      return current_index;
    }
    while(brackets!=0) {
      current_index++;
      l = get_line(current_index);
      if(l.get_cpu().equals(cpu) && l.get_pid().equals(pid)){
        if(l.last_element().equals("{")){
          brackets++;
        }
        if(l.last_element().equals("}")){
          brackets--;
        }
      }
    }
    return current_index;
  }
  
  int next_pid_function(int i, String cpu, String pid) {
    int current_index = i + 1;
    while(true) {
      line l = new line();
      try{l = get_line(current_index);}
      catch(Exception e) {return -1;}
    if(l.get_cpu().equals(cpu) && l.get_pid().equals(pid)){
        return current_index;
      }
      current_index++;
    }
  }
  
  
  void print_table() {
    for(int i = 0; i < lines.size(); i++){
      for(int j = 0; j < lines.get(i).get_length(); j++) {
        System.out.print(" " + lines.get(i).get_word(j));
      }
      System.out.println();
    }
  }
  
}