import java.util.ArrayList;
import java.lang.Double;

class line {
  private ArrayList<String> words = new ArrayList<String>();
  
  public static void main(String[] args) {
    line l = new line();
    l.add_word("test");
  }
  
  void add_word(String word) {
    words.add(word);
  }
  
  int get_length() {
    return words.size();
  }
  
  String get_word(int i) {
    return words.get(i);
  }
  
  String last_element() {
    return words.get(get_length() - 1);
  }
  
  double get_latency() {
    return Double.parseDouble(words.get(get_length() - 4));
  }
  
  String get_cpu() {
    return words.get(2);
  }
  
  String get_pid() {
    return words.get(1);
  }
  
  String get_function() {
    int size = get_length();
    if(words.get(size - 1).equals("{")){
      return words.get(size - 2);
    }
    return words.get(size-1);
  }
  
  boolean has_children() {
    return words.get(get_length()-1).equals("{");
  }
  
  void print_line() {
    for(String s: words){
      System.out.print(s + " ");
    }
    System.out.println();
    
  }
}