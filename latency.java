import java.util.ArrayList;
import java.lang.Integer;

class latency {
  private String function;
  private table t;
  private ArrayList<latencyinfo> latencies = new ArrayList<latencyinfo>();
  
  latency(table t, String function) {
    this.t = t;
    this.function = function;
  }
  
  latency() {
  }
  
  public static void main(String[] args) {
    latency test = new latency();
    test.run();
    if(test.parse_args(args)){
      return;
    }
    files f = new files();
    f.read_file(args[0]);
    latency l = new latency(f.get_table(), args[1]);
    l.populate_latencies();
    if(args.length == 2){
      l.print_lis();
    }
    if(args.length == 4){
      l.print_lis(Integer.parseInt(args[2]), Integer.parseInt(args[3]));
    }    
  }
  
  boolean parse_args(String[] args){
    if(args.length == 0){
      print_help();
      return true;
    }
    if(args.length != 2 && args.length != 4){
      System.out.println("Invalid option");
      System.out.println();
      System.out.println("latency:");
      print_help();
      return true;
    }
    if(!args[1].endsWith("()")){
      System.out.println("Function name must include brackets");
      return true;
    }
    return false;    
  }
  
  void print_help(){
    System.out.println("This outputs every instance of the specified function, along with the latency for each instance. Optionally, the user can specify the range within the text file that they want to see output from");
    System.out.println();
    System.out.println("usage:");
    System.out.println("java latency <tracing_data> <function> [<start_point>, <end_point>]");
    System.out.println("tracing_data: A text file which contains the output of trace-cmd's function graph tracer.");
    System.out.println("function: The function about which information will be presented (must include the end brackets).");
    System.out.println("start_point: A line number from tracing_data, no information about functions that start before this line will be presented.");
    System.out.println("end_point: A line number from tracing_data, no information about functions that end after this line will be presented.");
  }
      
  
  void run() {
    files f = new files();
    f.read_file("test.txt");
    t = f.get_table();
    add_latencyinfo(0);
    assert(latencies.get(0).get_end() == 6);
    add_latencyinfo(9);
    assert(latencies.get(1).get_end() == 9);
    latencies = new ArrayList<latencyinfo>();
    function = "__fdget_pos()";
    populate_latencies();
    assert(latencies.get(1).get_start() == 1);
    assert(latencies.get(0).get_start() == 0);
  }
  
  void populate_latencies() {
    int current_index = 0;
    int next_func = t.next_occurence(current_index, function);
    while(next_func != -1){
      current_index = next_func;
      add_latencyinfo(next_func);
      current_index++;
      next_func = t.next_occurence(current_index, function);
    }
  }
    
  
  void add_latencyinfo(int i) {
    int index = i;
    String pid = t.get_line(i).get_pid();
    double d;
    try{
      index = t.find_function_end(i);
      d = t.get_latency(i);
      latencyinfo li = new latencyinfo(i, index, d, pid);
      latencies.add(li);      
    }
    catch(Exception e){}
  }
  
  void print_lis(int start, int end) {
    for(latencyinfo li: latencies){
      if(li.get_start() > end) {
        return;
      }
      if(li.get_start() >= start && li.get_end() <= end){
        li.print_li();
        System.out.println();
      }
    }
  }    

  void print_lis(){
    for(latencyinfo li: latencies){
      li.print_li();
      System.out.println();
    }
  }    
      
  ArrayList<latencyinfo> get_latencies() {
    return latencies;
  }
}