import java.io.PrintWriter;
import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Double;

class files {
  
  private table t = new table();
  
  public static void main(String[] args) {
    files f = new files();
    f.run();
  }

  void run() {
    read_file("test.txt");
    line l;
    table testing = get_table();
    l = testing.get_line(0);
    assert(l.has_children());
    l = testing.get_line(3);
    Double d1 = new Double(l.get_latency());
    Double d2 = new Double("0.912");    
    assert(d1.equals(d2));
    assert(testing.next_occurence(0, "mutex_lock()") == 7);
    assert(testing.find_function_end(0) == 6);
    d1 = testing.get_latency(1);
    d2 = new Double("7.371");
    assert(d1.equals(d2));
    
  }      
  
  void read_file(String filename) {
    File f = new File(filename);
    Scanner s;
    try{
      s = new Scanner(f);
      read_lines(s);
    }
    catch(Exception e){
      throw new Error("File does not exist");
    }
  }    
  
  
  void read_lines(Scanner s){
    while(true){
      try{
        line l = new line();
        String[] data = s.nextLine().split("\\s+");
        for(String entry: data){
          l.add_word(entry);
        }
        t.add_line(l);
      }
      catch(Exception e){
        return;
      }
    }
  }

  
  table get_table() {
    return t;
  }
  
}
  