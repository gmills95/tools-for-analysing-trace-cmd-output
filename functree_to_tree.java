import treeminer.*;
import treeminer.util.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.ImmutablePair;

class functree_to_tree{
  private ArrayList<functree> functrees = new ArrayList<functree>();
  private TreeRepresentationUtils tr = new TreeRepresentationUtils();
  private TreeMiner tm = new TreeMiner();
  
  functree_to_tree(){
  }
  
  functree_to_tree(ArrayList<functree> functrees){
    this.functrees = functrees;
  }
  
  public static void main(String[] args) {
    files f = new files();
    f.read_file("test.txt");
    functree_to_tree t = new functree_to_tree();
    functree ft = new functree("__fdget_pos()", 1, f.get_table());
    functree ft2 = new functree("__fdget_pos()", 0, f.get_table());
    functree ft3 = new functree("__fdget_pos()", 18, f.get_table());
    ft.populate_children();
    ft2.populate_children();
    ft3.populate_children();
    t.add_functree(ft);
    t.add_functree(ft2);
    t.add_functree(ft3);
    ArrayList<String> freqs = t.get_trees(2);
    for(String s: freqs){
      System.out.println(s);
    }
  }
  
  int size(){
    return functrees.size();
  }
  
  void add_functree(functree f){
    functrees.add(f);
  }
  
  ArrayList<String> unique_subtrees(List<String> subtrees){
    ArrayList<String> uniques = new ArrayList<String>();
    for(String s: subtrees){
      if(single_element(s)){
        continue;
      }
      ArrayList<Integer> indexes = subtree_in_list(s, uniques);
      if(indexes.size() == 0){
        uniques.add(s);
        continue;
      }
      if(indexes.get(0) == -1){
        continue;
      }
      for(int i = 0; i < indexes.size(); i++){
        uniques.remove(indexes.get(i) - i);
      }
      uniques.add(s);
      for(String s1: uniques){
      }
    }
    return uniques;
  }
  
  ArrayList<String> get_trees(int min){
    ArrayList<String> fts = new ArrayList<String>();
    for(functree f: functrees){
      fts.add(get_tree(f));
    }
    return unique_subtrees(tm.findFrequentSubtrees(fts, min));
  }
  
  ArrayList<Integer> subtree_in_list(String subtree, ArrayList<String> subtrees){
    ArrayList<Integer> indexes = new ArrayList<Integer>();
    for(int i = 0; i < subtrees.size(); i++){
      if(tr.containsSubtree(subtree, subtrees.get(i))){
        indexes.add(i);
      }
      if(tr.containsSubtree(subtrees.get(i), subtree)){
        indexes.add(-1);
      }
    }
    return indexes;
  }    

  String get_tree(functree f){
    if(!f.has_children()){
      return f.get_function();
    }
    ArrayList<String> list = new ArrayList<String>();
    for(functree child: f.get_children()){
      list.add(get_tree(child));
    }
    return tr.addChildrenToNode(f.get_function(), list);
  }
  
  boolean single_element(String tree){
    String[] leaves = tree.split(" ");
    if(leaves.length == 1){
      return true;
    }
    return false;
  }
}